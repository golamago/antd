/// <reference types="react" />
import * as React from 'react';
import { TreeSelectProps } from './interface';
export { TreeSelectProps };
declare const _default: React.ComponentClass<TreeSelectProps>;
export default _default;
